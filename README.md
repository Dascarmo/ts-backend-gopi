# tracksale-api

Documentação produzida para o teste da Tracksale.

## Requisitos

Para o perfeito funcionamento do sistema, é preciso instalar os seguintes pacotes:

- MongoDB (para instalar, clique neste [link](https://docs.mongodb.com/manual/installation/))
- Node.JS (para instalar, clique neste [link](https://nodejs.org/en/download/package-manager/))

## Objetivo

Este sistema tem como objetivo calcular o número pi. A literatura adotada fora a da "série de Nilakantha". Com ela, é possível calcular quais são os termos que, somados, levam ao número pi. Para mais informações, acesse o [link](https://www.glc.us.es/~jalonso/exercitium/calculo-de-pi-mediante-la-serie-de-nilakantha/).

Os termos foram armazenados em um banco de dados de forma a não ser necessário o cálculo frequente do número pi. Sendo assim, basta chamar todos os termos dentro do banco (ou até o número de termos desejados) para que o valor seja fornecido.

A inserção de termos na base é feita através de uma chamada, bem como a sua remoção. Assim, a cada termo inserido, mais preciso o número pi será. Não foram especificados quais seriam os "inputs" e "outputs" do projeto. Então algumas considerações foram tomadas.

- Como os termos de pi podem ser calculados, as duas formas são possíveis. Caso o valor seja fornecido, ele será incluído na base, mesmo que seu valor esteja errado. Há chamadas para listar cada termo e para resetar os termos, assim é possível corrigir caso haja algum problema de cálculo.
- Quando o termo não é fornecido, ele é calculado de acordo com o index do termo. Seguindo a literatura, é possível calcular o termo apenas com base em seu index.
- Foram adicionados os termos em um banco de dados. Sendo assim, não será preciso que sejam calculadas quantas vezes a chamada for feita e sim apenas uma vez: quando forem inseridos dentro da base de dados. O somatório de todos resultará no resultado e quanto mais termos houver, mais preciso o número será.
- A saída para o cálculo do número pi possui dois valores: o valor de pi e qquantidade de termos utilizados. Assim, o usuário tem como saber qual a precisão empregada no cálculo do termo.

## Funcionalidades a serem implementadas

Algumas considerações foram feitas a cerca do que pode ser adicionado ao sistema para seu melhor funcionamento.

- Adicionar uma trava para que valores errôneos não fossem adicionados;
- Adicionar outras literaturas a fórmula do cálculo do pi;
- Criar perfis individuais para cálculos individuais do número pi;

## Documentação

Para acessar a documentação no Postman, clique [aqui](https://www.getpostman.com/collections/2b7190a2e34864c7aed4). Foram desenvolvidas 5 chamadas para esta API. São elas:

### Calculate PI
- Método: GET
- url: /pi
- Parâmetros: term_numbers(número, opcional)
- Descrição: chamada para retorno do número pi. Caso não seja passado o parâmetro 'term_numbers', será retornado a soma de todos os termos. Caso seja passado o parâmetro, será retornado a soma dos termos até atingir o número desejado ou todos os termos, caso o parâmetro peça um número de termos maior do que existe na base.

Response 200

> {
>    "pi": 3.1415419859977827,
>    "term_numbers": 16
> }

### List PI terms
- Método: GET
- url: /terms
- Parâmetros: -
- Descrição: chamada para retornar todos os termos que existem no banco que compõem o número pi.

Response 200

> [
    {
        "_id": "5b9110ec35f22241e48b9ecf",
        "term\_index": 0,
        "created\_at": "2018-09-06T11:35:08.332Z",
        "updated\_at": "2018-09-06T11:35:08.333Z",
        "value": 0.16666666666666666,
        "__v": 0
    }
]

### Insert PI term
- Método: POST
- url: /terms
- Parâmetros: term_index(número, opicional), value(número, opcional)
- Descrição: chamada para adição de um termo na base. O term\_index deve ser fornecido, caso queira uma inserção direta. Caso não seja fornecido, o mesmo será calculado (o termo de maior index presente no banco de dados acrescido de 1). Não pode haver termos de mesmo index na base. Caso não seja fornecido o valor do termo, o mesmo será calculado com base no index. A resposta é o objeto do termo inserido na base.

Body

>{
	"term\_index": 15
}

Response 200

> {
    "value": 0.16666666666666666,
    "term_index": 0dan
}

### Delete PI term
- Método: Delete
- url: /terms/:termId
- Parâmetros: termId(string, obrigatório)
- Descrição: Chamada para remover um termo da base.

Response 200

> {
    "message": "Termo removido com sucesso!"
}

### Reset PI
- Método: DELETE
- url: /terms
- Parâmetros: confirmation(número, opcional)
- Descrição: chamada para "resetar" o cálculo do valor de pi. Com esta chamada, todos os termos da série serão apagados. É preciso que o parâmetro "confirmation" esteja presente, assim evita-se "reset" desnecessário.

Response 200

> {
>    "success": true
> }

Response 412
> {
    "message": "Termos removidos com sucesso!"
}