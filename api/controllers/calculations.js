'use strict';

var mongoose = require('mongoose'),
HttpStatus = require('http-status-codes'),
PiInput = mongoose.model('PiInput');

function isPositiveNumeric(value) {
    return /^\d+$/.test(value);
}

exports.pi = function(req, res) {
    var pi = 3;
    var term_index = 0;
    var query = {};
    if(req["query"]["term_numbers"]){
        if(!isPositiveNumeric(req["query"]["term_numbers"])){
            res.status(HttpStatus.EXPECTATION_FAILED)
            .send({
                error: "O termo enviado não é um número válido!"
            })
            return 1;
        }
        query = {"term_index": {"$lte": parseInt(req["query"]["term_numbers"])-1}};
    }
    PiInput.find(query, function(err, piInputs) {
        if (err)
            res.send(err);
        piInputs.forEach(function(piInput, index, arr){
            pi += piInput.value;
            term_index += 1;
        });
        res.send({
            pi: pi,
            term_numbers: term_index
        })
    });
};