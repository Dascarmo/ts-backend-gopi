'use strict';

var mongoose = require('mongoose'),
HttpStatus = require('http-status-codes'),
PiInput = mongoose.model('PiInput');

exports.index = function(req, res) {
  PiInput.find({}, function(err, piInputs) {
    if (err)
      res.send(err);
    res.json(piInputs);
  });
};

exports.create = async function(req, res) {
  var new_term = new PiInput(req.body);
  new_term.term_index = await setIndex(new_term);
  if(!new_term.value){
    var aux = (new_term.term_index+1)*2,
    signal = 1;
    if(new_term.term_index%2==1)
      signal *= -1
    new_term.value = signal*4.0/(aux*(aux+1)*(aux+2))
  }
  console.log(new_term);
  PiInput.find({term_index: {"$eq": new_term["term_index"]}}, function(err, piInputs) {
    if (err)
      res.send(err);
    if(piInputs.length > 0){
      res.status(HttpStatus.PRECONDITION_FAILED).json({error: "Já existe um termo com este index!"})
    }else{
      new_term.save(function(err, piInput) {
          if (err)
            res.send(err);
          res.json({
            value: piInput.value,
            term_index: piInput.term_index
          });
      });
    }
  });
};

exports.destroy = function(req, res) {
    PiInput.remove({
      _id: req.params.termId
    }, function(err, piInput) {
      if (err)
        res.send(err);
      res.json({ message: 'Termo removido com sucesso!' });
    });
  };

exports.reset = function(req, res) {
  if(req["query"]["confirmation"] != "1"){
    res.status(HttpStatus.PRECONDITION_FAILED).json({ error: 'Você está tentando apagar todos os termos. Para isso, adicione o parâmetro "confirmation" com o valor "1".' });
  }else{
    PiInput.remove({}, function(err, piInput) {
      if (err)
        res.send(err);
      res.json({ message: 'Termos removidos com sucesso!' });
    });
  }
};

async function setIndex(new_term){
  if(!new_term.term_index){
    const piInput = await PiInput.findOne({}).sort('-term_index').exec();
    if (piInput != undefined) return piInput.term_index+1;
    else return 0;
  }else{
    return 0;
  }
}