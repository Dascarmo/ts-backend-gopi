'use strict';
module.exports = function(app) {
    var calculations = require('../controllers/calculations');
    var terms = require('../controllers/terms');

    // Pi Calculation Routes
    app.route('/pi')
        .get(calculations.pi);
    app.route('/terms')
        .get(terms.index)
        .post(terms.create)
        .delete(terms.reset);
    app.route('/terms/:termId')
        .delete(terms.destroy);
};