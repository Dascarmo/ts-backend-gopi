'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var PiInputSchema = new Schema({
  term_index: {
    type: Number,
    required: 'The index of pi calculation is needed'
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  value: {
    type: Number,
    required: 'The value of the number is needed'
  }
});

module.exports = mongoose.model('PiInput', PiInputSchema);